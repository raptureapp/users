<?php

namespace Rapture\Users\Tests;

use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Rapture\Users\UsersServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use InteractsWithViews;

    protected function getPackageProviders($app)
    {
        return [
            UsersServiceProvider::class,
        ];
    }
}
