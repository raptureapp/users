<?php

use Rapture\Users\Commands\CreateCommand;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

it('registers the command', function () {
    $packages = config('packages');
    $exists = array_key_exists('user:create', \Artisan::all());

    expect(array_key_exists('rapture/users', $packages))->toBe(true);
    expect($exists)->toBe(true);
});

it('creates a user with required arguments', function () {
    $command = new CreateCommand();

    DB::shouldReceive('table')->once()->with('users')->andReturnSelf();
    DB::shouldReceive('insert')->once()->andReturnTrue();

    $this->artisan($command->getName(), [
        'email' => 'test@example.com',
        'password' => 'password123',
    ])
    ->expectsOutput('User created')
    ->assertExitCode(0);
});

it('creates a user with optional name', function () {
    $command = new CreateCommand();

    DB::shouldReceive('table')->once()->with('users')->andReturnSelf();
    DB::shouldReceive('insert')->once()->andReturnTrue();

    $this->artisan($command->getName(), [
        'email' => 'test@example.com',
        'password' => 'password123',
        '--name' => 'John Doe',
    ])
    ->expectsOutput('User created')
    ->assertExitCode(0);
});

it('fails when missing required arguments', function () {
    $command = new CreateCommand();

    $this->expectException(Symfony\Component\Console\Exception\RuntimeException::class);
    $this->expectExceptionMessage('Not enough arguments (missing: "email, password").');

    $this->artisan($command->getName());
});

it('uses bcrypt for password hashing', function () {
    $command = new CreateCommand();
    $password = 'password123';

    DB::shouldReceive('table')->once()->with('users')->andReturnSelf();
    DB::shouldReceive('insert')->once()->withArgs(function ($arg) use ($password) {
        return password_verify($password, $arg['password']);
    })->andReturnTrue();

    $this->artisan($command->getName(), [
        'email' => 'test@example.com',
        'password' => $password,
    ])
    ->expectsOutput('User created')
    ->assertExitCode(0);
});

it('sets correct timestamps', function () {
    $command = new CreateCommand();
    $now = Carbon::now();
    Carbon::setTestNow($now);

    DB::shouldReceive('table')->once()->with('users')->andReturnSelf();
    DB::shouldReceive('insert')->once()->withArgs(function ($arg) use ($now) {
        return $arg['created_at'] == $now && $arg['updated_at'] == $now;
    })->andReturnTrue();

    $this->artisan($command->getName(), [
        'email' => 'test@example.com',
        'password' => 'password123',
    ])
    ->expectsOutput('User created')
    ->assertExitCode(0);

    Carbon::setTestNow();
});
