<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrackPasswordChange extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->datetime('password_changed_at')->nullable();
            $table->unsignedInteger('password_changed_by')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('password_changed_at');
            $table->dropColumn('password_changed_by');
        });
    }
}
