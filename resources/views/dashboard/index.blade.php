<x-dashboard :title="__('users::package.plural')">
    <x-heading class="flex justify-between items-center" :label="__('users::package.plural')">
        @can('users.create', 'users')
            <x-button href="{{ route('dashboard.users.create') }}" color="primary" icon="user-plus">
                <span>@langAction('new', __('users::package.singular'))</span>
            </x-button>
        @endcan
    </x-heading>

    <livewire:user-table />
</x-dashboard>
