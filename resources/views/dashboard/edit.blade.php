<x-dashboard :title="langAction('edit', __('users::package.singular'))">
    <x-heading class="flex justify-between items-center" :label="langAction('edit', __('users::package.singular'))">
        <a href="{{ route('dashboard.users.index') }}">
            <em class="far fa-reply mr-2"></em> @lang('rapture::actions.return')
        </a>
    </x-heading>

    <x-container>
        <x-form method="put" action="{{ route('dashboard.users.update', $user) }}">
            <div class="space-y-8">
                @hook('users.edit.before', compact('user'))

                <x-form-section title="Personal Information">
                    <div class="sm:flex space-y-4 sm:space-y-0 sm:space-x-6">
                        <x-input name="name" label="First Name" class="flex-1" :value="$user->name" required />
                        <x-input name="lname" label="Last Name" class="flex-1" :value="$user->lname" />
                    </div>

                    <x-input type="email" name="email" class="mt-4" :label="__('rapture::field.email')" required :value="$user->email" />

                    <x-combobox name="timezone" placeholder="- Select a timezone -" :options="$timezones" :value="$user->timezone ?? 'UTC'" label="Timezone" class="mt-4" />

                    @hook('users.edit.personal', compact('user'))
                </x-form-section>

                @hook('users.edit.between', compact('user'))

                <x-form-section title="Credentials" desc="Leave blank to keep the existing password.">
                    <x-password name="new_password" :label="__('rapture::field.password')" />

                    @hook('users.edit.credentials', compact('user'))
                </x-form-section>

                @if (count($groups) > 0)
                <x-form-section title="Groups" desc="Multiple groups may be applied." :wrap="false">
                    <div class="space-y-2">
                        @foreach ($groups as $group)
                            <x-box class="flex items-center justify-between">
                                <div>
                                    <p class="font-medium text-gray-900 dark:text-slate-200 cursor-pointer">{{ $group->name }}</p>
                                    @if ($group->description)
                                        <p class="text-sm text-gray-500 dark:text-slate-400 mt-1">{{ $group->description }}</p>
                                    @endif
                                </div>
                                <div class="flex-shrink-0">
                                    <x-toggle name="groups[]" value="{{ $group->id }}" :state="$userGroups->contains($group->id)" />
                                </div>
                            </x-box>
                        @endforeach
                    </div>
                </x-form-section>
                @endif

                @hook('users.edit.after', compact('user'))
            </div>

            <div class="mt-8 text-right">
                <x-button type="submit" size="large" color="primary">
                    @langAction('update', __('users::package.singular'))
                </x-button>
            </div>
        </x-form>
    </x-container>
</x-dashboard>
