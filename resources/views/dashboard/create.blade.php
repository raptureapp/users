<x-dashboard :title="langAction('new', __('users::package.singular'))">
    <x-heading class="flex justify-between items-center" :label="langAction('new', __('users::package.singular'))">
        <a href="{{ route('dashboard.users.index') }}">
            <em class="far fa-reply mr-2"></em> @lang('rapture::actions.return')
        </a>
    </x-heading>

    <x-container>
        <x-form method="post" action="{{ route('dashboard.users.store') }}">
            <div class="space-y-8">
                @hook('users.create.before')

                <x-form-section title="Personal Information">
                    <div class="sm:flex space-y-4 sm:space-y-0 sm:space-x-6">
                        <x-input name="name" label="First Name" class="flex-1" required />
                        <x-input name="lname" label="Last Name" class="flex-1" />
                    </div>

                    <x-input type="email" name="email" class="mt-4" :label="__('rapture::field.email')" required />

                    <x-combobox name="timezone" placeholder="- Select a timezone -" :options="$timezones" :value="auth()->user()->timezone ?? 'UTC'" label="Timezone" class="mt-4" />

                    @hook('users.create.personal')
                </x-form-section>

                @hook('users.create.between')

                <x-form-section title="Credentials" desc="Leave blank to have a password auto generated.">
                    <x-password :label="__('rapture::field.password')" />

                    <div class="relative flex items-start mt-6">
                        <div class="flex items-center h-5">
                            <x-checkbox id="sendCredentials" name="notify" value="yes" />
                        </div>
                        <div class="ml-3 text-sm leading-5">
                            <label for="sendCredentials">
                                <span class="font-medium text-gray-700 dark:text-slate-200">Email credentials</span>
                                <span class="block mt-1 text-gray-500 dark:text-slate-400">The user will receive an email with their username and password.</span>
                            </label>
                        </div>
                    </div>

                    @hook('users.create.credentials')
                </x-form-section>

                @if (count($groups) > 0)
                <x-form-section title="Groups" desc="Multiple groups may be applied." :wrap="false">
                    <div class="space-y-2">
                        @foreach ($groups as $group)
                            <x-box class="flex items-center justify-between">
                                <div>
                                    <p class="font-medium text-gray-900 dark:text-slate-200 cursor-pointer">{{ $group->name }}</p>
                                    @if ($group->description)
                                        <p class="text-sm text-gray-500 dark:text-slate-400 mt-1">{{ $group->description }}</p>
                                    @endif
                                </div>
                                <div class="flex-shrink-0">
                                    <x-toggle name="groups[]" value="{{ $group->id }}" />
                                </div>
                            </x-box>
                        @endforeach
                    </div>
                </x-form-section>
                @endif

                @hook('users.create.after')
            </div>

            <div class="mt-8 text-right">
                <x-button type="submit" size="large" color="primary">
                    @langAction('create', __('users::package.singular'))
                </x-button>
            </div>
        </x-form>
    </x-container>
</x-dashboard>
