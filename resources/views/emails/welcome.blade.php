@component('mail::message')
# Account Created

A new user account has been created for you at [{{ url('/') }}]({{ url('/') }}).

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="padding-top: 4px; padding-bottom: 4px;">Email:</td>
        <td style="padding-top: 4px; padding-bottom: 4px;" width="16">&nbsp;</td>
        <td style="padding-top: 4px; padding-bottom: 4px;"><strong>{{ $email }}</strong></td>
    </tr>
    <tr>
        <td style="padding-top: 4px; padding-bottom: 4px;">Password:</td>
        <td style="padding-top: 4px; padding-bottom: 4px;" width="16">&nbsp;</td>
        <td style="padding-top: 4px; padding-bottom: 4px;"><strong>{{ $password }}</strong></td>
    </tr>
</table>

@component('mail::button', ['url' => route('dashboard')])
Sign in
@endcomponent
@endcomponent
