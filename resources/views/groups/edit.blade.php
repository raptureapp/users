<x-dashboard :title="langAction('edit', __('users::groups.singular'))">
    <x-heading class="flex justify-between items-center" :label="langAction('edit', __('users::groups.singular'))">
        <a href="{{ route('dashboard.usergroups.index') }}">
            <em class="far fa-reply mr-2"></em> @lang('rapture::actions.return')
        </a>
    </x-heading>

    <x-container>
        <x-form method="put" action="{{ route('dashboard.usergroups.update', $usergroup) }}">
            <div class="space-y-10">
                @hook('usergroups.edit.before', compact('usergroup'))

                <x-form-section title="Group Information">
                    <x-input name="name" :value="$usergroup->name" :label="__('rapture::field.name')" />

                    <x-textarea name="description" :value="$usergroup->description" rows="5" :label="__('users::groups.description')" class="mt-4" />
                </x-form-section>

                @hook('usergroups.edit.after', compact('usergroup'))
            </div>

            <div class="mt-8 text-right">
                <x-button type="submit" size="large" color="primary">
                    @langAction('update', __('users::groups.singular'))
                </x-button>
            </div>
        </x-form>
    </x-container>
</x-dashboard>
