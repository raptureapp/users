<x-dashboard :title="__('users::groups.plural')">
    <x-heading class="flex justify-between items-center" :label="__('users::groups.plural')">
        @can('usergroups.create', 'groups')
            <x-quick-create :action="route('dashboard.usergroups.store')" icon="plus" :label="langAction('new', __('users::groups.singular'))" />
        @endcan
    </x-heading>

    <livewire:user-group-table />
</x-dashboard>
