# Users

Rapture's base user management system.

## Installation

This package is combined with the rapture core, but does require a few extra steps.

1. Replace the $fillable parameter within the User model with `protected $guarded = [];`
2. Add the `Rapture\Users\Traits\UserMethods` trait to the User model. 
3. Add `\Rapture\Users\Middleware\CheckSuspension::class` to `app/Http/Kernel.php`

## Usage

A web interface exists for all functionality. A few additional artisan commands exist if you are more comfortable within the command line.

### Commands

```bash
user:create {email} {password} {--N|name= : User Name}
user:list
```

## Extending

The following options are available if you would like to add to the user system. The artisan commands don't trigger any hooks or filters.

### Hooks

#### Events

In all cases the user model is passed through.

**user.stored** - Triggered during a new user creation
**user.updated** - Triggered during a user update
**user.deleted** - Triggered during a user deletion

#### Templating

##### Creation Screen

**users.create.before**
**users.create.between**
**users.create.after**
**users.create.personal**
**users.create.credentials**

##### Editing Screen

**users.edit.before**
**users.edit.between**
**users.edit.after**
**users.edit.personal**
**users.edit.credentials**

### Filters

Existing user rules are passed through prior to any database manipulation.

**users.store.validation** - Triggered during a new user creation
**users.update.validation** - Triggered during a user update
