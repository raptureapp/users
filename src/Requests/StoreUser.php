<?php

namespace Rapture\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Rapture\Hooks\Facades\Filter;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('users.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Filter::dispatch('users.store.validation', [
            'name' => 'required|max:255',
            'lname' => 'nullable',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'nullable|min:6',
        ]);
    }
}
