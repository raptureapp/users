<?php

namespace Rapture\Users\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Hooks\Facades\Hook;
use Rapture\Users\Events\UserGroupCreated;
use Rapture\Users\Events\UserGroupUpdated;
use Rapture\Users\Models\UserGroup;
use Rapture\Users\Requests\StoreUserGroup;
use Rapture\Users\Requests\UpdateUserGroup;

class UserGroupsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(UserGroup::class, 'usergroup');
    }

    public function index()
    {
        return view('users::groups.index');
    }

    public function store(StoreUserGroup $request)
    {
        $group = UserGroup::create([
            'name' => $request->input('name'),
        ]);

        Hook::dispatch('usergroup.stored', new UserGroupCreated($group));

        return redirect()
            ->route('dashboard.usergroups.index')
            ->with('status', langAlert('created', __('users::groups.singular')));
    }

    public function edit(UserGroup $usergroup)
    {
        return view('users::groups.edit', compact('usergroup'));
    }

    public function update(UpdateUserGroup $request, UserGroup $usergroup)
    {
        $usergroup->name = $request->input('name');
        $usergroup->description = $request->input('description');
        $usergroup->save();

        Hook::dispatch('usergroup.updated', new UserGroupUpdated($usergroup));

        return redirect()
            ->route('dashboard.usergroups.index')
            ->with('status', langAlert('updated', __('users::groups.singular')));
    }
}
