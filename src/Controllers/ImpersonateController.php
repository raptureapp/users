<?php

namespace Rapture\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

class ImpersonateController extends Controller
{
    public function impersonate(User $user)
    {
        if ($user->has2Factor() && !auth()->user()->has2Factor()) {
            return redirect()
                ->route('dashboard.users.index')
                ->withErrors('You need 2 factor enabled to impersonate this user.');
        }

        auth()->user()->impersonate($user);

        return redirect()->route('dashboard');
    }

    public function leaveImpersonate()
    {
        auth()->user()->leaveImpersonation();

        return redirect()->route('dashboard.users.index');
    }
}
