<?php

namespace Rapture\Users\Controllers;

use \DateTimeZone;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Rapture\Hooks\Facades\Hook;
use Rapture\Users\Events\UserCreated;
use Rapture\Users\Events\UserUpdated;
use Rapture\Users\Mail\Welcome;
use Rapture\Users\Models\UserGroup;
use Rapture\Users\Requests\StoreUser;
use Rapture\Users\Requests\UpdateUser;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index()
    {
        return view('users::dashboard.index');
    }

    public function create()
    {
        $timezones = collect(DateTimeZone::listIdentifiers())->mapWithKeys(function ($zone) {
            return [$zone => $zone];
        });

        $groups = UserGroup::orderBy('name')->get();

        return view('users::dashboard.create', compact('groups', 'timezones'));
    }

    public function store(StoreUser $request)
    {
        if ($request->filled('password')) {
            $password = $request->input('password', Str::random(32));
        } else {
            $password = Str::random(32);
        }

        $user = User::create([
            'name' => $request->input('name'),
            'lname' => $request->input('lname'),
            'email' => $request->input('email'),
            'timezone' => $request->input('timezone'),
            'password' => bcrypt($password),
        ]);

        $user->groups()->sync($request->input('groups', []));

        if ($request->filled('notify')) {
            Mail::to($request->input('email'))->send(new Welcome($user->email, $password));
        }

        Hook::dispatch('user.stored', new UserCreated($user));

        return redirect()
            ->route('dashboard.users.index')
            ->with('status', langAlert('created', __('users::package.singular')));
    }

    public function edit(User $user)
    {
        if ($user->id === auth()->user()->id) {
            return redirect()
                ->route('dashboard.users.index')
                ->withErrors([
                    'msg' => 'You are not allowed to edit your own account',
                ]);
        }

        $timezones = collect(DateTimeZone::listIdentifiers())->mapWithKeys(function ($zone) {
            return [$zone => $zone];
        });

        $userGroups = $user->groups->pluck('id');
        $groups = UserGroup::orderBy('name')->get();

        return view('users::dashboard.edit', compact('user', 'groups', 'userGroups', 'timezones'));
    }

    public function update(UpdateUser $request, User $user)
    {
        if ($user->id === auth()->user()->id) {
            return redirect()
                ->route('dashboard.users.index')
                ->withErrors([
                    'msg' => 'You are not allowed to edit your own account',
                ]);
        }

        if ($request->filled('new_password')) {
            $user->password = bcrypt($request->input('new_password'));
            $user->password_changed_at = now();
            $user->password_changed_by = auth()->user()->id;
        }

        $user->name = $request->input('name');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->timezone = $request->input('timezone');
        $user->save();

        $user->groups()->sync($request->input('groups', []));

        Hook::dispatch('user.updated', new UserUpdated($user));

        return redirect()
            ->route('dashboard.users.index')
            ->with('status', langAlert('updated', __('users::package.singular')));
    }
}
