<?php

namespace Rapture\Users\Policies;

use Rapture\Keeper\Policies\BasePolicy;

class UserPolicy extends BasePolicy
{
    protected $key = 'users';
}
