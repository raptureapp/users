<?php

namespace Rapture\Users\Policies;

use Rapture\Keeper\Policies\BasePolicy;

class UserGroupPolicy extends BasePolicy
{
    protected $key = 'usergroups';
}
