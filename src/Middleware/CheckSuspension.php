<?php

namespace Rapture\Users\Middleware;

use Closure;

class CheckSuspension
{
    public function handle($request, Closure $next)
    {
        if (auth()->check() && !is_null(auth()->user()->suspended_at)) {
            auth()->logout();

            return redirect()
                ->route('login')
                ->withErrors([
                    'msg' => 'Your account has been suspended.',
                ]);
        }

        return $next($request);
    }
}
