<?php

namespace Rapture\Users\Widgets;

use App\Models\User;
use Rapture\Core\Widgets\PieChart;

class UserAttrition extends PieChart
{
    public $label = 'User Attrition';
    public $purpose = 'Show a summary of users';

    public $description = 'Show a summary of users attrition';
    public $icon = 'user';
    public $permission = 'users.index';

    public function labels()
    {
        return [
            'Active',
            'Inactive',
            'Suspended',
        ];
    }

    public function data()
    {
        return [
            User::whereNull('suspended_at')->where('last_login_at', '>', now()->subDays(30))->count(),
            User::whereNull('suspended_at')->where('last_login_at', '<=', now()->subDays(30))->count(),
            User::whereNotNull('suspended_at')->count(),
        ];
    }
}
