<?php

namespace Rapture\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;
use Rapture\Hooks\Facades\Hook;
use Rapture\Packages\Package;
use Rapture\Packages\Providers\PackageProvider;
use Rapture\Users\Commands\CreateCommand;
use Rapture\Users\Commands\ListCommand;
use Rapture\Users\Livewire\UserGroupTable;
use Rapture\Users\Livewire\UserTable;
use Rapture\Users\Models\UserGroup;
use Rapture\Users\Policies\UserGroupPolicy;
use Rapture\Users\Policies\UserPolicy;
use Rapture\Users\Widgets\UserAttrition;

class UsersServiceProvider extends PackageProvider
{
    public function configure(Package $package)
    {
        $package->name('users')
            ->translations()
            ->routes('web')
            ->migrations()
            ->views()
            ->commands([
                CreateCommand::class,
                ListCommand::class,
            ])
            ->component('user-table', UserTable::class)
            ->component('user-group-table', UserGroupTable::class)
            ->widget(UserAttrition::class)
            ->installer();
    }

    public function installed()
    {
        Gate::policy(User::class, UserPolicy::class);
        Gate::policy(UserGroup::class, UserGroupPolicy::class);

        Hook::attach('dashboard.top.right', 'Rapture\Users\Listeners\ExitImpersonation');

        Event::listen('Illuminate\Auth\Events\Login', 'Rapture\Users\Listeners\TrackLogin');

        Builder::macro('createdAt', function ($timezone = null) {
            return $this->getModel()->created_at->timezone($timezone ?? auth()->user()->timezone());
        });

        Builder::macro('updatedAt', function ($timezone = null) {
            return $this->getModel()->updated_at->timezone($timezone ?? auth()->user()->timezone());
        });

        Builder::macro('localDate', function ($field, $timezone = null) {
            return $this->getModel()->$field->timezone($timezone ?? auth()->user()->timezone());
        });

        $this->suspensionMiddleware();
    }

    public function suspensionMiddleware()
    {
        $router = $this->app['router'];

        $router->pushMiddlewareToGroup('web', \Rapture\Users\Middleware\CheckSuspension::class);
    }
}
