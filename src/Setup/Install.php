<?php

namespace Rapture\Users\Setup;

use Rapture\Packages\BaseInstall;

class Install extends BaseInstall
{
    public function handle()
    {
        $this->addMenu([
            'label' => 'users::package.plural',
            'route' => 'dashboard.users.index',
            'icon' => 'users',
            'namespaces' => ['dashboard/users'],
            'position' => 999,
            'permission' => 'users.index',
        ]);

        $this->addSubMenu([
            'label' => 'users::groups.plural',
            'route' => 'dashboard.usergroups.index',
            'namespaces' => ['dashboard/usergroups'],
            'permission' => 'usergroups.index',
        ]);

        $this->resourcePermissions('users');
        $this->registerPermission('users', 'impersonate', 'Login as user');
        $this->registerPermission('users', 'suspend', 'Suspend / Reinstate');
        $this->resourcePermissions('usergroups');
    }

    public function prepareFiles()
    {
        $this->userModel();
    }

    public function userModel()
    {
        $modified = false;
        $userPath = base_path(str(config('auth.providers.users.model'))->replace('\\', '/') . '.php');
        $userModel = str(file_get_contents($userPath));

        if ($userModel->contains('$fillable')) {
            $userModel = $userModel->replace($userModel->betweenFirst('protected $fillable = [', ']'), '');
            $userModel = $userModel->replace('$fillable', '$guarded');
            $modified = true;
        }

        if (!$userModel->contains('UserMethods')) {
            $traits = $userModel->afterLast('use ')->before(';');
            $userModel = $userModel->replace($traits, $traits . ', UserMethods');

            $useStatements = $userModel->betweenFirst('use ', 'class User')->beforeLast(';') . ';';
            $userModel = $userModel->replace($useStatements, str($useStatements)->newLine()->append('use Rapture\Users\Traits\UserMethods;'));
            $modified = true;
        }

        if ($modified) {
            file_put_contents($userPath, $userModel);
        }
    }
}
