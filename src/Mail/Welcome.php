<?php

namespace Rapture\Users\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class Welcome extends Mailable
{
    use Queueable;

    public $email;
    public $password;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function build()
    {
        return $this->markdown('users::emails.welcome')
            ->subject('Welcome to ' . config('app.name'));
    }
}
