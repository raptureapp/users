<?php

namespace Rapture\Users\Livewire;

use App\Models\User;
use Rapture\Core\Columns\Date;
use Rapture\Core\Columns\ID;
use Rapture\Core\Columns\Image;
use Rapture\Core\Columns\Relationship;
use Rapture\Core\Columns\Text;
use Rapture\Core\Columns\TrueFalse;
use Rapture\Core\Livewire\DatatableComponent;
use Rapture\Core\Table\Action;
use Rapture\Core\Table\Column;
use Rapture\Core\Table\Exportable;
use Rapture\Core\Table\Operation;
use Rapture\Core\Table\Scope;
use Rapture\Hooks\Facades\Hook;
use Rapture\Users\Events\UserDeleted;
use Rapture\Users\Events\UserSuspended;
use Rapture\Users\Events\UserUnsuspended;
use Rapture\Users\Models\UserGroup;

class UserTable extends DatatableComponent
{
    use Exportable;

    public $table = 'dashboard.users';

    public $searchable = true;

    public function columns()
    {
        return [
            ID::make(),
            Column::make('preview', 'User')
                ->sortable('name')
                ->render(function ($user) {
                    return '<div class="flex items-center space-x-4"><img src="' . $user->avatar() . '" class="w-10 h-auto rounded-full" /><div><p class="font-medium">' . $user->fullName() . '</p><p class="mt-1 text-sm"><a href="mailto:' . $user->email . '" class="text-primary-600 underline">' . $user->email . '</a></p></div></div>';
                })
                ->dontExport()
                ->visible(),
            Image::make('avatar', 'Avatar')
                ->src(fn ($user) => $user->avatar())
                ->width('w-10')
                ->classes('w-20')
                ->circle(),
            Column::make('fname', 'Full Name')
                ->sortable('name')
                ->render(fn ($user) => $user->fullName()),
            Text::make('name', 'First Name'),
            Text::make('lname', 'Last Name'),
            Text::make('email', __('rapture::field.email'))
                ->url(fn ($user) => 'mailto:' . $user->email),
            TrueFalse::make('two_factor_confirmed_at', '2 Factor')
                ->nullable(),
            Relationship::make('group', __('users::groups.plural'))
                ->hasMany(UserGroup::class, 'groups', 'name'),
            Date::make('last_login_at', 'Last Login')
                ->visible(),
            Text::make('last_login_from', 'Last Login From'),
            Date::make('password_changed_at', 'Password Changed'),
            Text::make('password_changed_by', 'Password Changed By')
                ->render(function ($user) {
                    return $user->password_changed_by ? ($user->password_changed_by === $user->id ? 'User' : 'Admin') : '';
                }),
            Date::make('suspended_at', 'Suspended At')
                ->withinScope('suspended'),
            Date::make('created_at', __('rapture::field.created'))
                ->defaultSort(),
            Date::make('updated_at', __('rapture::field.updated')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('Impersonate')
                ->permission('users.impersonate')
                ->route('dashboard.users.impersonate')
                ->render('<em class="far fa-user-secret"></em>')
                ->condition(function ($user) {
                    if (session('impersonated_by') || $user->has2Factor() && !auth()->user()->has2Factor()) {
                        return false;
                    }

                    return $user->id !== auth()->user()->id && !$user->isSuspended();
                }),
            Action::make('Suspend User')
                ->permission('users.suspend')
                ->callback('suspend')
                ->render('<em class="far fa-lock w-5 text-center"></em>')
                ->condition(fn ($user) => $user->id !== auth()->user()->id && !$user->isSuspended()),
            Action::make('Unsuspend User')
                ->permission('users.suspend')
                ->callback('unsuspend')
                ->render('<em class="far fa-lock-open w-5 text-center"></em>')
                ->condition(fn ($user) => $user->id !== auth()->user()->id && $user->isSuspended()),
            Action::edit('users')
                ->primary()
                ->condition(fn ($user) => $user->id !== auth()->user()->id),
            Action::delete('users')
                ->condition(fn ($user) => $user->id !== auth()->user()->id && $user->id !== session('impersonated_by')),
        ];
    }

    public function operations()
    {
        return [
            Operation::make('Suspend')
                ->callback('massSuspend')
                ->permission('users.suspend')
                ->withinScope('active')
                ->render('<em class="far fa-lock"></em>'),
            Operation::make('Unsuspend')
                ->callback('massUnsuspend')
                ->permission('users.suspend')
                ->withinScope('suspended')
                ->render('<em class="far fa-lock-open"></em>'),
        ];
    }

    public function scopes()
    {
        return [
            Scope::make('active', 'Active')
                ->query(fn ($query) => $query->whereNull('suspended_at'))
                ->defaultScope(),
            Scope::make('suspended', 'Suspended')
                ->query(fn ($query) => $query->whereNotNull('suspended_at')),
        ];
    }

    public function massSuspend()
    {
        $this->authorize('users.suspend', auth()->user());

        User::whereIn('id', $this->selected)
            ->where('id', '!=', auth()->user()->id)
            ->update([
                'suspended_at' => now(),
            ]);

        $this->clearSelection();
    }

    public function massUnsuspend()
    {
        $this->authorize('users.suspend', auth()->user());

        User::whereIn('id', $this->selected)
            ->where('id', '!=', auth()->user()->id)
            ->update([
                'suspended_at' => null,
            ]);

        $this->clearSelection();
    }

    public function suspend(User $user)
    {
        $this->authorize('users.suspend', $user);

        Hook::dispatch('user.suspended', new UserSuspended($user));

        $user->suspended_at = now();
        $user->save();
    }

    public function unsuspend(User $user)
    {
        $this->authorize('users.suspend', $user);

        Hook::dispatch('user.unsuspended', new UserUnsuspended($user));

        $user->suspended_at = null;
        $user->save();
    }

    public function delete(User $user)
    {
        $this->authorize('users.destroy', $user);

        $user->purge();

        Hook::dispatch('user.deleted', new UserDeleted($user));
    }

    public function query()
    {
        return User::query();
    }
}
