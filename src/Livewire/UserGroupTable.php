<?php

namespace Rapture\Users\Livewire;

use Rapture\Core\Columns\Date;
use Rapture\Core\Columns\ID;
use Rapture\Core\Columns\Text;
use Rapture\Core\Livewire\DatatableComponent;
use Rapture\Core\Table\Action;
use Rapture\Users\Models\UserGroup;

class UserGroupTable extends DatatableComponent
{
    public $table = 'dashboard.usergroups';

    public $searchable = true;

    public function columns()
    {
        return [
            ID::make(),
            Text::make('name', 'Name')
                ->defaultSort()
                ->visible(),
            Text::make('description', 'Description')
                ->visible(),
            Text::make('users_count', 'Number of users')
                ->query(function ($query) {
                    $query->withCount('users');
                })
                ->visible(),
            Date::make('created_at', __('rapture::field.created')),
            Date::make('updated_at', __('rapture::field.updated')),
        ];
    }

    public function actions()
    {
        return [
            Action::edit('usergroups')
                ->primary(),
            Action::delete('usergroups'),
        ];
    }

    public function delete(UserGroup $group)
    {
        $this->authorize('usergroups.destroy', $group);

        $group->delete();
    }

    public function query()
    {
        return UserGroup::query();
    }
}
