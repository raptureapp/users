<?php

namespace Rapture\Users\Listeners;

use Illuminate\Auth\Events\Login;

class TrackLogin
{
    public function handle(Login $event)
    {
        if ($event->guard !== 'web') {
            return;
        }

        $event->user->update([
            'last_login_at' => now(),
            'last_login_from' => request()->getClientIp(),
        ]);
    }
}
