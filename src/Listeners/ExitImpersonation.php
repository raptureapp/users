<?php

namespace Rapture\Users\Listeners;

class ExitImpersonation
{
    public function handle()
    {
        if (!session('impersonated_by')) {
            return;
        }

        echo view('users::dashboard.impersonate');
    }
}
