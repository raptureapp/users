<?php

namespace Rapture\Users\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
