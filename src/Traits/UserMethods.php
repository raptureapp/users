<?php

namespace Rapture\Users\Traits;

use Illuminate\Support\Facades\Storage;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Scout\Searchable;
use Rapture\Users\Models\UserGroup;
use Rapture\Users\Models\UserMeta;

trait UserMethods
{
    use Impersonate, Searchable, TwoFactorAuthenticatable;

    public function meta()
    {
        return $this->hasMany(UserMeta::class);
    }

    public function groups()
    {
        return $this->belongsToMany(UserGroup::class);
    }

    public function scopeActive($query)
    {
        return $query->whereNull('suspended_at');
    }

    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lname' => $this->lname,
            'email' => $this->email,
        ];
    }

    public function defaultAvatar()
    {
        return 'https://ui-avatars.com/api/?name=' . urlencode($this->fullName())  . '&color=fff&background=475569';
    }

    public function avatar()
    {
        if (empty($this->avatar)) {
            return $this->defaultAvatar();
        }

        return Storage::url('avatars/' . $this->avatar);
    }

    public function fullName()
    {
        if (empty($this->lname)) {
            return $this->name;
        }

        return implode(' ', [$this->name, $this->lname]);
    }

    public function properName()
    {
        if (empty($this->lname)) {
            return $this->name;
        }

        return implode(', ', [$this->lname, $this->name]);
    }

    public function timezone()
    {
        return $this->timezone ?? 'UTC';
    }

    public function purge()
    {
        $this->delete();
        $this->meta()->delete();
    }

    public function isSuspended()
    {
        return !is_null($this->suspended_at);
    }

    public function has2Factor()
    {
        return !is_null($this->two_factor_confirmed_at);
    }
}
