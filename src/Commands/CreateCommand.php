<?php

namespace Rapture\Users\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create
                            {email : Email address}
                            {password : Password}
                            {--N|name= : User Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');
        $name = $this->option('name');

        if (!$email || !$password) {
            $this->error('Missing required arguments');
            return;
        }

        $timestamp = Carbon::now();

        $user = [
            'name' => 'User',
            'email' => $email,
            'password' => bcrypt($password),
            'created_at' => $timestamp,
            'updated_at' => $timestamp,
        ];

        if ($name) {
            $user['name'] = $name;
        }

        DB::table('users')->insert($user);

        $this->info('User created');
    }
}
