<?php

namespace Rapture\Users\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class UserUnsuspended
{
    use Dispatchable, SerializesModels;

    public function __construct(
        public User $user,
    ) {
    }
}
