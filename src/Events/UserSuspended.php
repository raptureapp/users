<?php

namespace Rapture\Users\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class UserSuspended
{
    use Dispatchable, SerializesModels;

    public function __construct(
        public User $user,
    ) {
    }
}
