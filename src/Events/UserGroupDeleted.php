<?php

namespace Rapture\Users\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rapture\Users\Models\UserGroup;

class UserGroupDeleted
{
    use Dispatchable, SerializesModels;

    public $group;

    public function __construct(UserGroup $group)
    {
        $this->group = $group;
    }
}
