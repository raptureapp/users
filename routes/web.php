<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Users\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('users', 'UsersController')->except('show', 'destroy');
        Route::resource('usergroups', 'UserGroupsController')->except('create', 'show', 'destroy');
        Route::get('impersonate/{user}', 'ImpersonateController@impersonate')->name('users.impersonate');
        Route::get('leave-impersonate', 'ImpersonateController@leaveImpersonate')->name('users.leave-impersonate');
    });
